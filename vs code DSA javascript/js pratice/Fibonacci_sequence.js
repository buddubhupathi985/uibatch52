// problem - Give a number 'n',find the first .n. elements of the fibonacci sequence

// in mathematics the fibonaccci sequence is asequence in which each number is the sum of the two preceding one.
// The first two numbers in the sequence are 0 and 1.
// fibonacci(2)=[0,1]
// fibonaccci(3)=[0,1,1]
// fibonaccci(7)=[0,1,1,2,3,5,8]

function fibonacci(n) {
    const sequence = [0, 1];
  
    for (let i = 2; i < n; i++) {
      const nextNumber = sequence[i - 1] + sequence[i - 2];
      sequence.push(nextNumber);
    }
  
    return sequence;
  }
  
  const n = 10; // Change this value to generate a sequence of a different length
  const result = fibonacci(n);
  console.log(result);

//   problem - Give an integer 'n',find the factorial of that integer
//   in mathematics,the factorial of a non-negative integer 'n',denoted n!,is the product of all 
//   positive integers less than or equal to 'n'.

//   Factorial(4)=4*3*2*1=24
//   Factorial(5)=5*4*3*2*1=120

function factorial(n) {
    
      let result = 1;
      for (let i = 2; i <= n; i++) {
        result *= i;
      }
      return result;
    }
  
  
 
  console.log( factorial(5));
  