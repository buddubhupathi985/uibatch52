var express = require('express');
var app = express();

app.get("/", function(req, res) {
    res.send("<h1>Connection Established Successfully...<h1>" + req.url);
});

app.listen(3000, function() {
    console.log("Server Started on Port Number: 3000");
    console.log("Please Visit the Site, URL: http://localhost:3000");
});
