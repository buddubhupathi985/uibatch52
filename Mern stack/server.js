var express = require('express');
var app = express();

var bodyparser = require('body-parser');
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));

// app.use("/",require("./components/home"));
// app.use("/",require("./components/employees"));
// app.use("/",require("./components/empindex"));
// app.use("/",require("./components/empid"));
app.use("/fetch",require("./components/fetch"));
app.use("/register",require("./components/register"));
app.use("/register2",require("./components/register2"));
app.use("/register3",require("./components/register3"));
app.use("/login",require("./components/login"));
app.use("/del",require("./components/del"));

app.server = app.listen(3000, function() {
    console.log("Server Started on Port Number: 3000");
    console.log("URL: http://localhost:3000");
});

