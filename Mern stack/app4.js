var express = require('express');
var app = express();
var dataFile = require('../data/data.json');


/*Default Path/URL : localhost:3000*/
app.get("/", function(req, res) {
    res.send("<h1>Connection Established Successfully... URL: " + req.url + "<h1>");
});

/*Get All Employees*/
app.get("/employees", function(req, res) {
    var employeesList = [];
    dataFile.employees.forEach(function(employee) {
        employeesList.push(employee);
    });
    res.send(employeesList);
});

/*Get Employee based on Index value*/
app.get("/employees/:index", function(req, res) {
    var employee = dataFile.employees[req.params.index];
    res.send(employee);
});


app.server = app.listen(3000, function() {
    console.log("Server Started on Port Number: 3000");
    console.log("URL: http://localhost:3000");
});