var express = require('express');
var app = express();



var dataFile = require('../data/data.json');


let empindex = express.Router().get("/:index", function(req, res) {
    var employee = dataFile.employees[req.params.index];
    res.send(employee);
});

module.exports = empindex;