var express=require('express');
var app=express();

var dataFile = require('../data/data.json');


let employees=express.Router().get("/employees", function(req, res) {
    var employeesList = [];
      dataFile.employees.forEach(function(employee) {
        employeesList.push(employee);
    }); 
    res.send(employeesList);
});

module.exports = employees;
