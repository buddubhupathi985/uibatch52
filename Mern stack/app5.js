var express = require('express');
var app = express();
var dataFile = require('../data/data.json');


/*Default Path/URL : localhost:3000*/
app.get("/", function(req, res) {
    res.send("<h1>Connection Established Successfully... URL: " + req.url + "<h1>");
});

/*Get All Employees*/
app.get("/getAllEmployees", function(req, res) {
    var employeesList = [];
    dataFile.employees.forEach(function(employee) {
        employeesList.push(employee);
    });
    res.send(employeesList);
});

/*Get Employee based on Index value*/
app.get("/getEmployeeByIndex/:index", function(req, res) {
    var employee = dataFile.employees[req.params.index];
    res.send(employee);
});

/*Get Employee Based on EmployeeID => localhost:3000/employees/101 */
app.get("/getEmployeeById/:empId", function(req, res) {
    var empId = req.params.empId;
    var emp = {"empId":0, "empName":"Record Not Found", "salary":0.0};

    dataFile.employees.forEach(function(employee) {
        if (employee.id == empId) {
           emp = employee; 
        }
    });

    res.send(emp);

});

app.server = app.listen(3000, function() {
    console.log("Server Started on Port Number: 3000");
    console.log("URL: http://localhost:3000");
});