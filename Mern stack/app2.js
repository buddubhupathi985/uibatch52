cd 
var express = require('express');
var app = express();

var dataFile = require('../data/data.json');

app.get("/", function(req, res) {
    var employeesList = [];

    dataFile.employees.forEach(function(employee) {
        employeesList.push(employee);
    });

    res.send(employeesList);

});

app.server = app.listen(3000, function() {
    console.log("Server Started on Port Number: 3000");
    console.log("URL: http://localhost:3000");
});