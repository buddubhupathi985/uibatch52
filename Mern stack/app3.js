var express = require('express');
var app = express();
var dataFile = require('../data/data.json');

app.get("/:index", function(req, res) {
    var employee = dataFile.employees[req.params.index];
    res.send(employee);
});

app.server = app.listen(3000, function() {
    console.log("Server Started on Port Number: 3000");
    console.log("URL: http://localhost:3000/indexValue");
});
