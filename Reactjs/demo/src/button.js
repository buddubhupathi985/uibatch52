import React, { Component } from 'react';

class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 0,
    };
  }

  increaseNumber = () => {
    this.setState({ number: this.state.number + 1 });
  };

  decreaseNumber = () => {
    this.setState({ number: this.state.number - 1 });
  };

  render() {
    const { number } = this.state;

    return (
      <div>
        <h1>Number: {number}</h1>
        <button onClick={this.decreaseNumber}>-</button>
        <button onClick={this.increaseNumber}>+</button>
      </div>
    );
  }
}

export default Button;
