import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Test from './test';
import Header from './Header';
import Body from './Body';
import Footer from './Footer';
import wish from './Wish1';
import Statee from './stateex';
import Changestate from './changestate';
import Button from './button';
import Emplyee from './emplyee';
import Ex3 from './ex3';
import TestCase from './TestCase';
import Exjs4 from './exjs4';
import Exspread5 from './exspread5';
import Reflogin from './reflogin';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css'
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {/* <Ex3/> */}
    <Reflogin/>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
