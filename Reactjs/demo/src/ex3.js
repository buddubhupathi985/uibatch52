import react,{useState} from 'react'
const Ex3 =()=>{
    const [message,setMessage] = useState('hi this function object');
    const changeMessage =()=>{
        setMessage('message changed')
    }
return(
    <div>
        {message}
        <button onClick={changeMessage}>Change Message </button>
    </div>
)
    
}
export default Ex3