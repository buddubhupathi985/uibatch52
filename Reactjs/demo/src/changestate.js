import React, { Component } from 'react'

export default class Changestate extends Component {
    constructor(){
        super();//it caal the state object from parent component and component is parent
        this.state={
            message : 'Hi this Buddu Bhupathi'
        }
    }
    changeMessage(){
        //this.state.message="Hi This text is changed..";
        this.setState(
            {
               message:"Hi THis text is changed" 
            }
        )
    }
  render() {
    return (
      <div>
        <h1>{this.state.message}</h1>
        <button onClick={()=>this.changeMessage()}>Change Text</button>

      </div>
    )
  }
}
