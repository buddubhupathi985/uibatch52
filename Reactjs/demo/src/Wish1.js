import React from 'react'

const Wish = ({name,subject}) => {
  return (
    <div>
      <h1>Hello! {name}</h1>
      <h2>learning {subject}</h2>
    </div>
  )
}

export default Wish
