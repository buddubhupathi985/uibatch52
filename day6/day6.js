//factorial problems

function factorial(n) {
   let hello=1;
    for (let i=1;i<=n;i++){
        hello =hello*i;  
    }
     return hello;
}
console.log(factorial(5));

//even and odd number

function evenorodd(num){
    
    if (num%2==0)
        return "even";
    else
        return "odd";
    
}
console.log(evenorodd(2000000001))

//reverse method for str

function reverseString(str) {
    return str.split("").reverse().join("");
}
console.log(reverseString(" hello ")); // Output: "olleh"
console.log(reverseString(" JavaScript ")); // Output: "tpircSavaJ"

//reverse method for number

function reverseNumber(number) {
    return parseInt(number.toString().split('').reverse().join(''));
  }
console.log(reverseNumber(12345)); 


function reversenumber(number) {
    return parseInt(number.toString().split('').reverse().join(''));
}
console.log(reversenumber(7077559625));

//number is a prime or not

/*Certainly! Let's dive into more detail about the loop condition for (let i = 2; i <= Math.sqrt(number); i++) and
 the logic behind if (number % i === 0) to check for prime numbers.

Loop Condition Explanation:

javascript
Copy code
for (let i = 2; i <= Math.sqrt(number); i++)
let i = 2: We start the loop with i equal to 2 because we want to check for divisors starting from 2, 
as 1 is not considered in this check (since all numbers are divisible by 1).

i <= Math.sqrt(number): The loop will continue as long as the value of i is less than or equal to the square root of the number.
 This condition is essential for an efficient prime number check.

Explanation: When we look for divisors of a number, we don't need to check beyond its square root. 
If a number has a divisor greater than its square root, it must also have a corresponding divisor 
smaller than its square root. For example, if a number is divisible by 10, 
it must also be divisible by 2 or 5, which are smaller divisors. So, 
checking up to the square root is sufficient to find all possible divisors.

Logic Behind if (number % i === 0):

In the loop, we use the modulo operator % to check if the number is divisible by the current value of i.
 If number % i === 0 evaluates to true, it means that the number is divisible by i, and i is a divisor of the number.

If we find any such divisor (i.e., number is divisible by i), 
then the number cannot be a prime number because it has at least one other positive divisor (other than 1 and itself).

In this case, the loop is terminated prematurely by return false,
 and the function returns false to indicate that the number is not prime.

Example:

Let's take the number 15 as an example to walk through the loop and the logic:

number = 15.

The loop starts with i = 2 and continues as long as i <= Math.sqrt(15).

Math.sqrt(15) is approximately 3.87. So, the loop will run for i = 2, i = 3, and then stop because i = 4
 would be greater than Math.sqrt(15).

For each value of i in the loop:

When i = 2: 15 % 2 is 1 (not divisible).
When i = 3: 15 % 3 is 0 (divisible).
Since we found a divisor (3) other than 1 and 15, we immediately return false, indicating that 15 is not a prime number.

In conclusion, the loop iterates from i = 2 up to the square root of the number, 
checking if the number is divisible by any of these values.
 If it finds any such divisor, it returns false, indicating that the number is not prime.
  If the loop completes without finding any divisors, 
the number is prime, and the function returns true*/

//checking prime or not
function isPrime(number) {
    if (number <= 1) {
      return false;
    }
  
    for (let i = 2; i <= Math.sqrt(number); i++) {
      if (number % i === 0) {
        return false;
      }
    }
  
    return true;
  }
  console.log(isPrime(15));
  console.log(isPrime(2));

//higher order function


/*map() creates a new array from calling a function for every array element.

map() does not execute the function for empty elements.

map() does not change the original array.*/

const number = [1, 2, 3, 4, 5];
const squares = number.map((num) => num ** 2);
console.log(squares);

/*We have an array called numbers containing numeric values: [1, 2, 3, 4, 5].

We use the map function on the numbers array. The map function takes a callback function as an argument,
 and this function will be applied to each element of the array.

Inside the map function, we have an arrow function (num) => .... This function takes a single argument num, 
which represents each number from the numbers array.

Now, let's break down the expression inside the arrow function step by step:

num * 2: This part multiplies each number in the array by 2.
The map function will process each number in the numbers array using the arrow function,
 and it will create a new array called doubledNumbers.

The resulting doubledNumbers array will contain the transformed elements, 
where each number in the original array has been doubled.

For example, starting with the numbers array [1, 2, 3, 4, 5], 
the map function will perform the following operations:

For num = 1, num * 2 results in 1 * 2 = 2.
For num = 2, num * 2 results in 2 * 2 = 4.
For num = 3, num * 2 results in 3 * 2 = 6.
For num = 4, num * 2 results in 4 * 2 = 8.
For num = 5, num * 2 results in 5 * 2 = 10.
The doubledNumbers array will be [2, 4, 6, 8, 10].

So, using the map function, we were able to create a new array with each 
element in the original array multiplied by 2, effectively doubling the numbers in the
numbers array without modifying the original array. The map function is a powerful
tool for performing such transformations on arrays.*/


//filter  
/*The filter() method creates a new array filled with elements that pass a test provided by a function.

The filter() method does not execute the function for empty elements.

The filter() method does not change the original array.*/
let ages = [32, 33, 16, 40];
function checkAdult(ages){
    return ages >= 18;
}
const result = ages.filter(checkAdult);
console.log(result)

//problem1
function isSubstringPresent(sentence, substring) {
  return sentence.includes(substring);
}

const sentence = "hello world";
const substring = "word";

const isPresent = isSubstringPresent(sentence, substring);
console.log(isPresent);

//problem2
function filterStringsByLength(stringsArray, minLength) {
  return stringsArray.filter((str) => str.length > minLength);
}

// Example usage:
const strings = ["apple", "banana", "grape", "orange", "kiwi"];
const minLength = 5;

const filteredStrings = filterStringsByLength(strings, minLength);
console.log(filteredStrings);

let arr = [1.5, 20.3, 11.1, 40.7];
//reduce method
// Callback function for reduce method
function sumofArray(sum, num) {
    return sum + Math.round(num);
}
 
//Fucntion to execute reduce method
function myGeeks(item) {
    // Display output
    console.log(arr.reduce(sumofArray, 0));
}

//data types
//let,var,const
//conditional statements
//if,ifelse,switch
//for loop each
//while
//problems statements by using above concepts
//git
//arrow functiion,higher order function
 
//questions

//find cube of each element in an array
//return only odd elements in an array
//product of each element in an array
//find the element which are more than then given specififed value
//["hello","hii","bye"]===hello

let num = [1, 2, 3, 4, 5];
let cube = number.map((num) => num ** 3);
console.log(cube);

let oddnumbers=number.filter((num)=>num%2!=0);
console.log(oddnumbers);

let product=number.reduce((pro,num)=>pro*num,1);
console.log(product)

let words=["hello","hii","bye"]
let words_filter=words.filter((word)=>word.length>3);
console.log(words_filter)



