//factor
function findFactors(number) {
    if (number <= 0) {
      return "Factors are defined only for positive integers.";
    }
  
    const factors = [];
    for (let i = 1; i <= number; i++) {
      if (number % i === 0) {
        factors.push(i);
      }
    }
  
    return factors;
}
console.log(findFactors(5));  

//perfect number
function isPerfectNumber(number) {
    if (number <= 0) {
      return false; 
    }
  
    let sumOfDivisors = 0;
    for (let i = 1; i <= number / 2; i++) {
      if (number % i === 0) {
        sumOfDivisors += i;
      }
    }
  
    return sumOfDivisors === number;
  }
  
  console.log(isPerfectNumber(28));


  //trim
let str = "  hello world ";
let trimmedstr = str.trim();
console.log(trimmedstr);

//countwords
function countwords(sentence){

  const words = sentence.split(' ');
  return `The number of words are ${words.length}`;

}

let sentence = "hello i am learning javascript";
console.log(countwords(sentence));


//sum of negative numbers

function sumnegativevalues(arr) {
  let sum=0;
  for(let i=0;i<arr.length;i++) {
    if (arr[i]<0){
    sum = sum + arr[i] ;
  }  
}
return sum;
}
const numbers = [-1,2,3,-2,-5,10]
const result = sumnegativevalues(numbers);
console.log(result);
 
//by using for each loop
function sumOfNegativeValues(arr){
  let sumofposi =0;
  let sumofneg =0
arr.forEach((element) => {
  if(element <0 ){
      sumofneg+=element
  }
  else{
     sumofposi+=element
  }
    
});

return `The Sum of Negative is ${sumofneg} and Sum of Position ${sumofneg}`

}

let arr = [1,-1,2,3,-4,5,8]
console.log(sumOfNegativeValues(arr));


//problem1
//write a program to find wether the given substring is present in the given sentence
//input hello world ,word
//output true

//problem2
//you have been given an array of stringhs and a specificd length value write a function
// athat filters the string from the array keeping those strings whose length is greater 
//than the specified length 


//imput :["java","python","html","css",],4
//output should return the array elements >4
//["pthyon"]

//prepare MAP,FILTER,REDUCE and you need to give presentation on these topuics,how to use 



